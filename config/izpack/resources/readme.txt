   Instructions for running the TKI CLI client:

   The program is accessible via the created shortcut or via the command line.
   If running via the command line the executable is located at "bin/tki".
   If "add to user PATH" was set during the installation.  The tki command is available in any command shell via the
    command "tki"

   To view the available command line options, run the command "tki -help"

   The TKI CLI client adds an entry into the aws credentials file and sets the current shell's profile to
    AWS_DEFAULT_PROFILE={ALIAS,ROLE}

   Run AWS command line tool as usual from the current shell window to use the credentials created by the TKI CLI client