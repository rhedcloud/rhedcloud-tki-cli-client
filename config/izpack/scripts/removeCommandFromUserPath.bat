@echo OFF
setlocal ENABLEEXTENSIONS

set pathToRemove=%~1

@rem Put 3rd argument (data parameter) from querying registry into pathData variable
FOR /F "USEBACKQ tokens=2*" %%A IN (`REG QUERY HKCU\Environment /v Path 2^> nul`) DO (
    set lineDefined=%%A
    set pathData=%%B
)

if defined lineDefined (
    call set newPath=%%pathData:%pathToRemove%;=%%
) else (
    set newPath=%pathData%;
)

Reg delete HKCU\Environment /v Path /f > nul
Reg add HKCU\Environment /v Path /t REG_EXPAND_SZ /d "%newPath%" /f > nul

endlocal