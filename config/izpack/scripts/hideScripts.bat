@echo OFF
setlocal ENABLEEXTENSIONS

set pathToHide=%~1

attrib +h "%pathToHide%"

endlocal