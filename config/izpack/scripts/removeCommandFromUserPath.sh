#!/bin/bash
#bin bash because sh does not support substitution in ubuntu
arg1=${1//\//\\\/}
unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)    theFile=.bash_profile;;
    *)          theFile=.bashrc
esac
sed -e "/export PATH=\$PATH:$arg1/d" -i "$HOME/$theFile"
