#!/usr/bin/env bash
appDir=${1//\"/};
mkdir "$appDir/Contents/Resources";
cp /Applications/Utilities/Terminal.app/Contents/Resources/Terminal.icns "$appDir/Contents/Resources/RHEDcloudTKI.icns";
touch "$appDir";