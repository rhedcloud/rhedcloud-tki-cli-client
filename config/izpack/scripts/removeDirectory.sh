#!/usr/bin/env bash
for var in "$@"
do
    directory=${var//\"/};
    rm -rf "$directory";
done

