#!/usr/bin/env bash
installDir=${1//\"/}
cp -rf  "`pwd`/dist/jre" "$installDir"

# when running the installer from UpdateApply.groovy the execute permissions seem to get lost
# probably a umask issue but couldn't track it down so just fix it up manually.

chmod -f u+x "$installDir"/jre/bin/*
chmod -f u+x "$installDir"/jre/lib/*.dylib
chmod -f u+x "$installDir"/jre/lib/server/*.dylib
chmod -f u+x "$installDir"/jre/lib/jspawnhelper
