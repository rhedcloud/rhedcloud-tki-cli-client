#!/usr/bin/env sh
unameOut="$(uname -s)"
case "${unameOut}" in
    Darwin*)    theFile=.bash_profile;;
    *)          theFile=.bashrc
esac
printf "\nexport PATH=\$PATH:$1\n" >> "$HOME/$theFile"