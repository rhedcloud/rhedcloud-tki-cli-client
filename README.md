# The RHEDcloud Temporary Key Issuance CLI Client

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description

A utility for issuing temporary AWS keys from the command line. 

## AWS Setup ##

The TKI CLI Client has a few features that require specific setup in AWS.

The first feature is encryption of the users password in the messages sent to the TKI Service.
The second feature allows the client to detect when a new version has been released and install
it if the user desires.  Both features rely on AWS users and policies. 

#### Emory AWS 8 ####

The Emory AWS 8 account (emory-aws-8) has been setup with separate users and policies generally split
between the TKI CLI Client and Service and per environment.  For example, there are users named `tki-client-dev` and
`tki-service-dev` for the DEV environment with a similar naming for TEST and STAGE.  The PROD users are named
`tki-client-account` and `tki-service-account` which is consistent with other names in production.

The `tki-service-*` accounts are not used in the client and will not be discussed further here other than to say
they grant access to the TKI Service to retrieve the private key used to decrypt the users password.

The `tki-client-*` users (via the `EmoryTkiClient*Policy` policies) grant access to both
the public key (for encryption) and the S3 buckets that hold the TKI CLI Client installer bundles and the
VERSION.txt file (see bitbucket-pipelines.yml).  These users do not allow console login but there
are access keys issued for API access.  They are stored in the `application.properties` file (and bundled with the
installed program) under the property names `tki.client.ENV.aws.secretsmanager.accessKeyId`
and `tki.client.ENV.aws.secretsmanager.secretAccessKey`.

To wrap up this section, here is an example for the `tki-client-test` user.
It has the following directly attached policy named `EmoryTkiClientTestPolicy`:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "SecretsManagerPermissions",
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": "arn:aws:secretsmanager:us-east-1:134285882893:secret:tki-public-key-TEST.der-aBcDe"
        },
        {
            "Sid": "S3Permissions",
            "Effect": "Allow",
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::emory-aws-test-tki-client/VERSION.txt",
                "arn:aws:s3:::emory-aws-test-tki-client/linux-test-installer.tar.gz",
                "arn:aws:s3:::emory-aws-test-tki-client/mac-test-installer.tar.gz",
                "arn:aws:s3:::emory-aws-test-tki-client/windows-test-installer.zip"
            ]
        }
    ]
}
```

#### Master Accounts ####

The S3 buckets referenced in the `EmoryTkiClient*Policy` policies are contained in the Master accounts
for the environment.  So, continuing the example from above, the `emory-aws-test-tki-client` bucket
is contained in the `aws-test-master@emory.edu` account. The bucket policy grants permissions for
a few different purposes but the section that is important to the TKI CLI Client allows the `tki-client-*`
users to get objects from the bucket.  Here is the section for the TEST environment:

```json
{
    "Effect": "Allow",
    "Principal": {
        "AWS": "arn:aws:iam::134285882893:user/tki-client-stage"
    },
    "Action": "s3:GetObject",
    "Resource": [
        "arn:aws:s3:::emory-aws-stage-tki-client/VERSION.txt",
        "arn:aws:s3:::emory-aws-stage-tki-client/linux-stage-installer.tar.gz",
        "arn:aws:s3:::emory-aws-stage-tki-client/mac-stage-installer.tar.gz",
        "arn:aws:s3:::emory-aws-stage-tki-client/windows-stage-installer.zip"
    ]
}
```

## Quick Start ##

Build a distribution package, then run the application with the launcher script.

```
./gradlew deploy
./build/deploy/dev/tki
```

Run the following to view optional command line arguments
```
./build/deploy/dev/tki -help
```

The installer files will be created at ./deploy/dev/jar
To run the installer
```
java -jar deploy/dev/jar/tki-client-dev-installer.jar
```

A jre bundled installer is created at .deploy/dev/windows, .deploy/dev/linux, .deploy/dev/mac
To run the installer on a system without java, run the following file
  - windows: set_up.exe
  - linux: install.sh
  - mac: setup.app

## Build Process ##

This Tki CLI Client Application is built and deployed using the provided Gradle wrapper.
Ensure Java 1.8 is on the System path with `java -version`, and then run `./gradlew tasks`
or `./gradlew.bat tasks` to get a list of build commands.
Below is a list of a few helpful tasks and their description.

**build** -
 Default task. Runs all build, check, and test tasks, before assembling the final distro
 packages for Windows and Unix style systems.

**eclipse** or **idea** -
 Generate project files for your IDE

**deploy** -
 Custom task in build task to prepare release artifacts in the deploy directory


## SOAP Setup ##

The TKI CLI client application runtime depends on a remote RPC service defined
in the `rhedcloud-tki-service` project.
The client-side service proxy and generated WSDL classes can be found in the jars,
*rhedcloud-tkiservice-webservice-1.0-client-classes.jar* and
*rhedcloud-tkiservice-webservice-1.0-wsdl-classes.jar* located
at *openeai-servicegen/target/tkiservice/rhedcloud-tkiservice-webservice/build/lib*
after running the `./gen-webservice tkiservice` command in the `Generate web service facade` step.

## Components ##

**MainView** -
 Primary view controller and implementation
 of the [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/) `org.springframework.boot.CommandLineRunner`

**Application** -
 This is the main entry point of the application, and serves as the top level IoC Container
 for any static Bean definitions.

**TkiClientService** -
 A wrapper for remote RPC methods like `SecurityAssertion.Generate` and
`RoleAssumption.Generate` plus error handling.

**LocalFileService** -
 Handle reading and writing `.aws/credentials` and `.aws/config` files.

**edu.emory.tki.client.prompts** -
 This package contains command line prompting for required information.
