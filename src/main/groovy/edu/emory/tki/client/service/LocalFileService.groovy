package edu.emory.tki.client.service

import org.beryx.textio.TextIO
import org.beryx.textio.TextTerminal
import org.ini4j.Config
import org.ini4j.Ini
import org.rhedcloud.www.tkiservice.Credentials
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

/**
 * Handle local filesystem services.
 */
@Component
class LocalFileService {

    private static final Logger logger = LoggerFactory.getLogger(LocalFileService.class)

    @Autowired
    private TextIO textIO

    void writeAwsCredentialsSection(String accountAlias, String role, Credentials credentials, Integer duration) {
        Path credentialsFile = getCredentialsPath()

        if (Files.notExists(credentialsFile)) {
            createMissingFile(credentialsFile)
        }

        Ini ini = newIni(credentialsFile.toFile())

        String sectionName = "tki-${accountAlias}-${role}"
        ini.put(sectionName, "aws_access_key_id", credentials.accessKeyId)
        ini.put(sectionName, "aws_secret_access_key", credentials.secretAccessKey)
        ini.put(sectionName, "aws_session_token", credentials.sessionToken)

        ini.store(credentialsFile.toFile())

        /* explain to the user what we've done */
        TextTerminal terminal = textIO.getTextTerminal()
        terminal.println("Section ${sectionName} updated in ${credentialsFile.toAbsolutePath().toString()} .")
        terminal.println("You can now use AWS CLI in this command shell.")
        terminal.println("If using the credentials from another command shell, use the command:")
        terminal.println("        aws --profile \"${sectionName}\"")
        terminal.println("The credentials will be valid for ${duration} seconds.")
    }

    String readDefaultAwsRegion() {
        Path configFile = getConfigPath()

        if (Files.notExists(configFile)) {
            createMissingFile(configFile)
        }

        Ini ini = newIni(configFile.toFile())
        return ini.get('default', 'region')
    }

    void writeAwsRegion(String accountAlias, String role, String region) {
        Path configFile = getConfigPath()

        if (Files.notExists(configFile)) {
            createMissingFile(configFile)
        }

        Ini ini = newIni(configFile.toFile())

        String sectionName = "profile tki-${accountAlias}-${role}"
        ini.put(sectionName, "region", region)

        ini.store(configFile.toFile())
    }

    private Ini newIni(File file) {
        // setup config suitable to AWS configuration files
        Config.getGlobal().headerComment = false
        Config.getGlobal().escape = false

        return new Ini(file)
    }

    /**
     * Write a file that contains the role chosen by the user.
     *
     * It is used by the start script (tki or tki.bat) to set the AWS_DEFAULT_PROFILE environment variable.
     *  See build.gradle where it modifies the start script to do this.
     *
     * @param alias AWS account alias
     * @param role chosen role
     */
    void writeTmpRoleFile(String alias, String role) {
        // write the file to user.home since we're guaranteed to be able to write there
        Path tmpRoleFile = Paths.get(System.getProperty('user.home'), 'tki_aws_default_profile.txt')
        Files.write(tmpRoleFile, "tki-${alias}-${role}".getBytes(), StandardOpenOption.CREATE)
    }

    private Path getConfigPath() {
        String envConfigFile = System.getenv('AWS_CONFIG_FILE')
        if (envConfigFile) {
            return Paths.get(envConfigFile)
        }

        return Paths.get(System.getProperty('user.home'), '.aws', 'config')
    }

    private Path getCredentialsPath() {
        String envCredentialsFile = System.getenv('AWS_SHARED_CREDENTIALS_FILE')
        if (envCredentialsFile) {
            return Paths.get(envCredentialsFile)
        }

        return Paths.get(System.getProperty('user.home'), '.aws', 'credentials')
    }

    private void createMissingFile(Path file) {
        String message = "${file.toAbsolutePath().toString()} does not exist, creating it."
        logger.info(message)
        TextTerminal terminal = textIO.getTextTerminal()
        terminal.println(message)
        Files.createDirectories(file.getParent())
        Files.createFile(file)
    }
}
