package edu.emory.tki.client.service

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.secretsmanager.AWSSecretsManager
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult
import edu.emory.tki.client.exceptions.AwsAccountServiceDownException
import edu.emory.tki.client.exceptions.DuoAuthRejectedException
import edu.emory.tki.client.exceptions.DuoAuthTimeoutException
import edu.emory.tki.client.exceptions.DuoPasscodeIncorrectException
import edu.emory.tki.client.exceptions.DuoPreAuthFailedException
import edu.emory.tki.client.exceptions.DuoUserNotAllowedException
import edu.emory.tki.client.exceptions.TkiServiceDownException
import edu.emory.tki.client.exceptions.TokenExpiredException
import edu.emory.tki.client.exceptions.TokenMaxTimeException
import edu.emory.tki.client.exceptions.UnrecoverableException
import edu.emory.tki.client.exceptions.UserNamePasswordIncorrectException
import org.apache.axis2.AxisFault
import org.apache.axis2.client.Options
import org.apache.axis2.transport.http.HTTPConstants
import org.apache.commons.httpclient.Header
import org.openeai.www.openeai.Result
import org.rhedcloud.tkiservice.webservice.TkiServiceStub
import org.rhedcloud.www.tkiservice.AccountAlias
import org.rhedcloud.www.tkiservice.Credential
import org.rhedcloud.www.tkiservice.Device
import org.rhedcloud.www.tkiservice.RoleAssumption
import org.rhedcloud.www.tkiservice.RoleAssumptionGenerateRequest
import org.rhedcloud.www.tkiservice.RoleAssumptionReply
import org.rhedcloud.www.tkiservice.RoleAssumptionRequisition
import org.rhedcloud.www.tkiservice.RoleDetail
import org.rhedcloud.www.tkiservice.SecurityAssertion
import org.rhedcloud.www.tkiservice.SecurityAssertionGenerateRequest
import org.rhedcloud.www.tkiservice.SecurityAssertionReply
import org.rhedcloud.www.tkiservice.SecurityAssertionRequisition
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.crypto.Cipher
import java.nio.ByteBuffer
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec

/**
 * Wrapper for appropriate remote RPC methods in the generated SOAP service class found in `rhedcloud-tki-service`
 */
@Service
class TkiClientService  {

    private static final Logger logger = LoggerFactory.getLogger(TkiClientService.class)

    @Value('${tki.client.tkiservice.webservice.timeout.ms}')
    Integer tkiServiceTimeout

    @Value('${tki.client.tkiservice.webservice.url.${tki.client.env}}')
    String tkiServiceUrl

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.accessKeyId}')
    private String accessKeyId

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.secretAccessKey}')
    private String secretAccessKey

    @Value('${tki.client.publicKeySecretName}')
    private String publicKeySecretName

    private Cipher cipher
    private TkiServiceStub tkiService

    @PostConstruct
    void postConstructInitialization() {
        //byte[] publicKeyBytes = StreamUtils.copyToByteArray(getClass().getResourceAsStream("/tki-public-key-gen.der"))

        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretAccessKey)))
                .withRegion("us-east-1") // all secrets are in this region
                .build()

        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(publicKeySecretName)
        GetSecretValueResult getSecretValueResult = client.getSecretValue(getSecretValueRequest)
        ByteBuffer secretBinaryBytes = getSecretValueResult.getSecretBinary()
        byte[] publicKeyBytes = new byte[secretBinaryBytes.capacity()]
        secretBinaryBytes.get(publicKeyBytes, 0, publicKeyBytes.length)

        X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyBytes)
        KeyFactory publicKeyFactory = KeyFactory.getInstance("RSA")
        PublicKey publicKey = publicKeyFactory.generatePublic(spec)

        cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)

        // fall back to local testing if the URL isn't set
        String url = (tkiServiceUrl != null) ? tkiServiceUrl : "http://localhost:8080/axis2/services/TkiService"
        tkiService = new TkiServiceStub(url)
        tkiService._getServiceClient().getOptions().setTimeOutInMilliSeconds(tkiServiceTimeout)
    }

    /**
     * 
     * @param request
     * @return
     */
    SecurityAssertion samlLogin(String userName, String password) {
        logger.info("samlLogin Execution Begin")

        setBasicAuth(userName, password)
        SecurityAssertionReply securityAssertionReply
        try {
            securityAssertionReply = tkiService.securityAssertionGenerate(createLoginRequest(userName, password))
        } catch (AxisFault e) {
            if (e.getMessage().contains("401 Error")) {
                throw new UserNamePasswordIncorrectException("Received 401 error due to incorrect HTTP BASIC auth")
            } else {
                logger.error("Unknown Axis exception", e)
                throw e
            }
        }

        if (securityAssertionReply.result.status == 'failure') {
            logger.info("samlLogin failure detected")
            reportError(securityAssertionReply.result)
        }

        SecurityAssertion securityAssertion = securityAssertionReply.securityAssertion[0]

        if (securityAssertion.roleDetail == null || securityAssertion.roleDetail.length < 1) {
            // we're not certain that AWS Account Service is down but that's usually what this means
            throw new AwsAccountServiceDownException("No roles returned from server.  This could mean the AwsAccountService is down.  Please try again later.")
        }

        logger.info("samlLogin Execution End")
        return securityAssertion
    }

    void smsPasscodeRequest(String userName, String password, String deviceId) {
        logger.info("smsPasscodeRequest Execution Begin")
        setBasicAuth(userName, password)
        SecurityAssertionReply securityAssertionReply = tkiService.securityAssertionGenerate(createSmsRequest(userName, deviceId))
        if (securityAssertionReply.result.status == 'failure') {
            logger.info("smsPasscodeRequest failure detected")
            reportError(securityAssertionReply.result)
        }
        logger.info("smsPasscodeRequest Execution End")
    }

    /**
     *
     * @param request
     * @return
     */
    RoleAssumption assumeRole(String userName, String password,
                              RoleDetail roleDetail, Device device, String authFactor, Integer passcode,
                              String region, Integer timeout) {

        logger.info("assumeRole Execution Begin")

        setBasicAuth(userName, password)
        RoleAssumptionReply roleAssumptionReply = tkiService.roleAssumptionGenerate(createRoleAssumptionRequest(
                userName,
                roleDetail,
                device,
                authFactor,
                passcode,
                region,
                timeout))

        if (roleAssumptionReply.result.status == 'failure') {
            logger.info("assumeRole failure detected")
            reportError(roleAssumptionReply.result)
        }

        RoleAssumption roleAssumption = roleAssumptionReply.roleAssumption[0]

        logger.info("assumeRole Execution End")
        return roleAssumption
    }

    private void setBasicAuth(String userName, String password) {
        Options options = tkiService._getServiceClient().getOptions()
        List<Header> headers = (List<Header>) options.getProperty(HTTPConstants.HTTP_HEADERS)
        if (headers == null) {
            headers = new ArrayList<Header>()
        }
        // get rid of any existing Authorization header
        headers.removeAll { it.name == "Authorization" }

        String hash = (userName + ":" + password).getBytes("UTF-8").encodeBase64().toString().trim()
        headers.add(new Header("Authorization", "Basic ${hash}"))
        options.setProperty(HTTPConstants.HTTP_HEADERS, headers)
    }

    /**
     * Fills out a SecurityAssertionGenerateRequest object from the provided user/pass combo
     */
    private SecurityAssertionGenerateRequest createLoginRequest(String userName, String userPass) {

        String encryptedPassword = Base64.getEncoder().encodeToString(cipher.doFinal(userPass.getBytes("UTF-8")))

        SecurityAssertionGenerateRequest request = new SecurityAssertionGenerateRequest()
        request.securityAssertionRequisition = new SecurityAssertionRequisition()
        request.securityAssertionRequisition.type = 'netid/password'
        request.securityAssertionRequisition.principal = userName
        request.securityAssertionRequisition.credential = new Credential()
        request.securityAssertionRequisition.credential.type = 'password'
        request.securityAssertionRequisition.credential.secret = encryptedPassword

        return request
    }

    private SecurityAssertionGenerateRequest createSmsRequest(String userName, String deviceId) {
        SecurityAssertionGenerateRequest request = new SecurityAssertionGenerateRequest()
        request.securityAssertionRequisition = new SecurityAssertionRequisition()
        request.securityAssertionRequisition.type = 'duo'
        request.securityAssertionRequisition.principal = userName
        request.securityAssertionRequisition.credential = new Credential()
        request.securityAssertionRequisition.credential.type = 'sms'
        request.securityAssertionRequisition.credential.id = deviceId

        return request
    }

    /**
     * Fills out a RoleAssumptionGenerateRequest object
     */
    private RoleAssumptionGenerateRequest createRoleAssumptionRequest(String userName,
                                                                      RoleDetail roleDetail,
                                                                      Device device,
                                                                      String authFactor,
                                                                      Integer passcode,
                                                                      String region,
                                                                      Integer timeout) {
        /*
        <RoleAssumptionRequisition>
            <Principal>jenny</Principal>
            <Region>us-east-1</Region>
            <RoleDetail>
              <AccountAlias>  <!-- not used but must be given -->
                  <AccountId>123456789012</AccountId>
                  <Name>aws-test-1</Name>
              </AccountAlias>
              <PrincipalArn>arn:aws:iam::123456789012:saml-provider/Emory_IDP</PrincipalArn>
              <RoleArn>arn:aws:iam::123456789012:role/rhedcloud/RHEDcloudAuditorRole</RoleArn>
            </RoleDetail>
            <Credential>
                <Type>push</Type>
                <Id>optional DeviceId</Id>
            </Credential>
            <DurationSeconds>43200</DurationSeconds>
        </RoleAssumptionRequisition>
        */
        RoleAssumptionGenerateRequest request = new RoleAssumptionGenerateRequest()
        request.roleAssumptionRequisition = new RoleAssumptionRequisition()
        request.roleAssumptionRequisition.principal = userName
        request.roleAssumptionRequisition.region = region
        request.roleAssumptionRequisition.roleDetail = new RoleDetail()
        request.roleAssumptionRequisition.roleDetail.accountAlias = new AccountAlias()
        request.roleAssumptionRequisition.roleDetail.accountAlias.accountId = "not used"
        request.roleAssumptionRequisition.roleDetail.accountAlias.name = "not used"
        request.roleAssumptionRequisition.roleDetail.principalArn = roleDetail.principalArn
        request.roleAssumptionRequisition.roleDetail.roleArn = roleDetail.roleArn
        request.roleAssumptionRequisition.credential = new Credential()
        if (!passcode) {
            request.roleAssumptionRequisition.credential.type = authFactor
        } else {
            request.roleAssumptionRequisition.credential.type = 'passcode'
            request.roleAssumptionRequisition.credential.secret = passcode
        }
        request.roleAssumptionRequisition.credential.id = device.deviceId
        request.roleAssumptionRequisition.durationSeconds = timeout

        return request
    }

    private void reportError(Result result) {
        if (result.error[0].type == 'Application') {
            // these errors came from the TKI service so we can parse them
            // (there is a bug in tki-service where the error always goes into the errorDescription instead of actually using the error structure)
            String[] errorLines = result.error[0].errorDescription.split("\n")
            if (errorLines.length < 4) {
                // not in the expected format so just dump error
                String error = result.error[0].errorDescription
                logger.error("Unexpected error in from TkiService: " + error)
                throw new UnrecoverableException(error)
            }

            int errorNum = 0
            List<Exception> exceptions = []
            for (int i=2; i+1 < errorLines.length; i += 3) {

                String errCode = errorLines[i].replace("Error ["+errorNum+"] Number: ", "")
                String errDescription = errorLines[i+1].replace("Error ["+errorNum+"] Description: ", "")
                errorNum++

                logger.info("Tki Service error received: Number: ${errCode}, Description: ${errDescription}")
                switch (errCode) {
                    case 'TKI-3004':
                        exceptions.add(new TkiServiceDownException(errDescription))
                        break
                    case 'TKI-3020':
                    case 'TKI-3021':
                        exceptions.add(new UserNamePasswordIncorrectException(errDescription))
                        break
                    case 'TKI-4010':
                        exceptions.add(new TokenExpiredException(errDescription)) //happens if user sets timeout too small
                        break
                    case 'TKI-4011':
                    case 'TKI-4012':
                        exceptions.add(new TokenMaxTimeException(errDescription)) //happens if user sets timeout longer than 5 mins
                        break
                    case 'TKI-5005':
                        exceptions.add(new DuoAuthTimeoutException(errDescription))
                        break
                    case 'TKI-5003':
                    case 'TKI-5004':
                        exceptions.add(new DuoAuthRejectedException(errDescription))
                        break
                    case 'TKI-5001':
                        exceptions.add(new DuoUserNotAllowedException(errDescription))
                        break
                    case 'TKI-5002':
                        exceptions.add(new DuoPreAuthFailedException(errDescription))
                        break
                    case 'TKI-5006':
                        exceptions.add(new DuoPasscodeIncorrectException(errDescription))
                        break
                    default:
                        logger.error("Unexpected error in from TkiService: Number: ${errCode}, Description: ${errDescription}")
                        exceptions.add(new UnrecoverableException("Number: ${errCode}, Description: ${errDescription}"))
                }
            }

            throw exceptions.get(0)
        } else {
            String error = result.error[0].errorDescription
            logger.error("Unexpected error in from TkiService: " + error)
            throw new UnrecoverableException(error)
        }
    }
}
