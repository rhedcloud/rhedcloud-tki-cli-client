package edu.emory.tki.client.exceptions

class TkiServiceDownException extends RuntimeException {
    TkiServiceDownException(String message) {
        super(message)
    }
}
