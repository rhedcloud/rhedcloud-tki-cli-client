package edu.emory.tki.client.exceptions

class UnrecoverableException extends RuntimeException {
    UnrecoverableException(String message) {
        super(message)
    }
}
