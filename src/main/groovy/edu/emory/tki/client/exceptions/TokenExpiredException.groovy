package edu.emory.tki.client.exceptions

class TokenExpiredException extends RuntimeException {
    TokenExpiredException(String message) {
        super(message)
    }
}
