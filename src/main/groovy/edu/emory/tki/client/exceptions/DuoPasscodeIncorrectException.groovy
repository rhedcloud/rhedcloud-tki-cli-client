package edu.emory.tki.client.exceptions

class DuoPasscodeIncorrectException extends RuntimeException {
    DuoPasscodeIncorrectException(String message) {
        super(message)
    }
}
