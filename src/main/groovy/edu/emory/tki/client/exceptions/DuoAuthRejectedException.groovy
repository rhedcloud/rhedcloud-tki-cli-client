package edu.emory.tki.client.exceptions

class DuoAuthRejectedException extends RuntimeException {
    DuoAuthRejectedException(String message) {
        super(message)
    }
}
