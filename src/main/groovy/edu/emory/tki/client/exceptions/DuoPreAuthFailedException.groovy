package edu.emory.tki.client.exceptions

class DuoPreAuthFailedException extends RuntimeException {
    DuoPreAuthFailedException(String message) {
        super(message)
    }
}
