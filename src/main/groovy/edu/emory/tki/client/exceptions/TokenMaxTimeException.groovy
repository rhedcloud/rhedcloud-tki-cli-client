package edu.emory.tki.client.exceptions

class TokenMaxTimeException extends RuntimeException {
    TokenMaxTimeException(String message) {
        super(message)
    }
}
