package edu.emory.tki.client.exceptions

class AwsAccountServiceDownException extends RuntimeException {
    AwsAccountServiceDownException(String message) {
        super(message)
    }
}
