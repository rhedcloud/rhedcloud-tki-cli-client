package edu.emory.tki.client.exceptions

class DuoUserNotAllowedException extends RuntimeException {
    DuoUserNotAllowedException(String message) {
        super(message)
    }
}
