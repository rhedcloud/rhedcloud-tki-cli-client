package edu.emory.tki.client.exceptions

class UserNamePasswordIncorrectException extends RuntimeException {
    UserNamePasswordIncorrectException(String message) {
        super(message)
    }
}
