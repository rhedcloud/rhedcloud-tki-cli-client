package edu.emory.tki.client.exceptions

class DuoAuthTimeoutException extends RuntimeException {
    DuoAuthTimeoutException(String message) {
        super(message)
    }
}
