package edu.emory.tki.client.exceptions

class UserExitException extends RuntimeException {
    UserExitException(String message) {
        super(message)
    }
}
