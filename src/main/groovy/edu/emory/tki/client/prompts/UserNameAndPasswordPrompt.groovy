package edu.emory.tki.client.prompts

import edu.emory.tki.client.exceptions.UserExitException
import edu.emory.tki.client.exceptions.UserNamePasswordIncorrectException
import edu.emory.tki.client.state.StateEnum
import org.beryx.textio.InputReader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Prompt for username and password.
 */
@Component
class UserNameAndPasswordPrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(UserNameAndPasswordPrompt.class)

    void prompt() {
        InputReader usernameInputReader = textIO.newStringInputReader()
        InputReader passwordInputReader = textIO.newStringInputReader()
                .withInputMasking(true)
        InputReader retryInputReader = textIO.newBooleanInputReader()
                .withTrueInput("y").withFalseInput("n").withDefaultValue("n")

        while (!stateManager.get(StateEnum.O_SECURITY_ASSERTION)
                && stateManager.get(StateEnum.I_NUM_RETRIES_USER_NAME_PASSWORD) >= 0) {

            logger.info("UserNameAndPasswordPrompt Prompt Execution Start")

            String userName = stateManager.get(StateEnum.S_PRINCIPAL)
            String userPass = stateManager.get(StateEnum.S_PASSWORD)

            try {
                if (!userName) {
                    userName = usernameInputReader.read("Username")
                    stateManager.put(StateEnum.S_PRINCIPAL, userName)
                    logger.info("Prompted user for username, username entered.")
                }

                if (!userPass) {
                    userPass = passwordInputReader.read("Password")
                    stateManager.put(StateEnum.S_PASSWORD, userPass)
                    logger.info("Prompted user for password, password entered.")
                }

            } catch (UserNamePasswordIncorrectException e) {

                Integer numRetries = stateManager.get(StateEnum.I_NUM_RETRIES_USER_NAME_PASSWORD) as Integer

                stateManager.putAll([
                        (StateEnum.S_PRINCIPAL) : null,
                        (StateEnum.S_PASSWORD) : null,
                        (StateEnum.I_NUM_RETRIES_USER_NAME_PASSWORD) : (numRetries - 1)
                ])

                if (numRetries > 0) {
                    logger.info("UserNamePasswordIncorrectException, retrying prompt.")
                    // the default value comes back as a string, otherwise we get back a Boolean
                    Object retryInput = retryInputReader.read("Incorrect Username / Password combination.  Try again? (Y/N)")
                    boolean retry = (retryInput instanceof String) ? (((String) retryInput) == "Y") : ((Boolean) retryInput)
                    if (!retry) {
                        logger.info("UserNamePasswordIncorrectException, user chose not to retry.")
                        throw new UserExitException('User has chosen to exit the program')
                    }
                } else {
                    logger.info("UserNamePasswordIncorrectException, no more retries left.")
                    throw e
                }
            } finally {
                logger.info("UserNameAndPasswordPrompt Prompt Execution End")
            }
        }
    }
}
