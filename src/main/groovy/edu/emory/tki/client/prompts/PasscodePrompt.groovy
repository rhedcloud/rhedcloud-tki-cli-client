package edu.emory.tki.client.prompts

import edu.emory.tki.client.exceptions.DuoPasscodeIncorrectException
import edu.emory.tki.client.exceptions.UserExitException
import edu.emory.tki.client.state.StateEnum
import org.beryx.textio.InputReader
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Prompt for DUO passcode.
 */
@Component
class PasscodePrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(PasscodePrompt.class)

    void prompt() {

        InputReader readRetryReader = textIO.newBooleanInputReader()
                .withTrueInput('Y').withFalseInput('N').withDefaultValue('N')

        while (stateManager.get(StateEnum.S_AUTH_FACTOR)
                && !stateManager.get(StateEnum.B_PASSCODE_ACCEPTED)
                && stateManager.get(StateEnum.I_NUM_RETRIES_PASSCODE) >= 0) {

            logger.info("PasscodePrompt Prompt Execution Start")

            try {
                if (stateManager.get(StateEnum.S_AUTH_FACTOR) == 'sms') {

                    Integer passcode = textIO.newIntInputReader()
                            .withMinVal(1)
                            .read("Passcode from SMS")

                    logger.info("Prompted user for passcode, passcode entered.")
                    stateManager.put(StateEnum.I_PASSCODE, passcode)
                }

                stateManager.put(StateEnum.B_PASSCODE_ACCEPTED, Boolean.TRUE)

            } catch (DuoPasscodeIncorrectException e) {

                Integer numRetries = stateManager.get(StateEnum.I_NUM_RETRIES_PASSCODE) as Integer
                if (numRetries > 0) {
                    logger.info("DuoPasscodeIncorrectException, retrying prompt.")

                    // the default value comes back as a string, otherwise we get back a Boolean
                    Object readRetryInput = readRetryReader.read("Incorrect Passcode.  Try again? (Y/N)")
                    boolean doRetry = (readRetryInput instanceof String) ? (((String) readRetryInput) == "Y") : ((Boolean) readRetryInput)
                    if (doRetry) {
                        stateManager.putAll([
                                (StateEnum.I_PASSCODE)            :null,
                                (StateEnum.B_PASSCODE_ACCEPTED)   :Boolean.FALSE,
                                (StateEnum.I_NUM_RETRIES_PASSCODE):(numRetries - 1)
                        ])
                    } else {
                        logger.info("DuoPasscodeIncorrectException, user chose not to retry.")
                        throw new UserExitException('User has chosen to exit the program')
                    }
                } else {
                    logger.info("DuoPasscodeIncorrectException, no more retries left.")
                    throw e
                }

            } finally {
                logger.info("PasscodePrompt Prompt Execution End")
            }
        }
    }
}
