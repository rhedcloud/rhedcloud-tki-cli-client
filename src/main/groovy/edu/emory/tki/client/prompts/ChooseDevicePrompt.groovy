package edu.emory.tki.client.prompts

import edu.emory.tki.client.state.StateEnum
import org.rhedcloud.www.tkiservice.Device
import org.rhedcloud.www.tkiservice.SecurityAssertion
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Prompt for DUO device.
 */
@Component
class ChooseDevicePrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(ChooseDevicePrompt.class)

    void prompt() {
        if (stateManager.get(StateEnum.O_SECURITY_ASSERTION) && !stateManager.get(StateEnum.O_CHOSEN_DEVICE)) {
            logger.info("ChooseDevicePrompt Prompt Execution Start")

            SecurityAssertion securityAssertion = stateManager.get(StateEnum.O_SECURITY_ASSERTION) as SecurityAssertion
            if (securityAssertion.device.length == 1) {

                logger.info("Only one device capability choosing ${securityAssertion.device[0].displayName}")
                // no need to choose when there is only one
                stateManager.put(StateEnum.O_CHOSEN_DEVICE, securityAssertion.device[0])
            } else {
                Map<String, Device> deviceMap = securityAssertion.device.collectEntries {
                    [(it.displayName): it]
                }

                List<String> deviceKeyList = new ArrayList<>(deviceMap.keySet())

                if (stateManager.get(StateEnum.B_AUTO_PICK_DUO_AUTH_DEVICES)) {

                    logger.info("Auto parameter is set,  choosing ${deviceKeyList.get(0)}")
                    stateManager.put(StateEnum.O_CHOSEN_DEVICE, deviceMap[(deviceKeyList.get(0))])

                } else {
                    String deviceString = textIO.newStringInputReader()
                            .withNumberedPossibleValues(true)
                            .withPossibleValues(deviceKeyList)
                            .withDefaultValue(deviceKeyList.get(0))
                            .read("Available Devices")

                    logger.info("Prompted user for device, user chose ${deviceString}")
                    stateManager.put(StateEnum.O_CHOSEN_DEVICE, deviceMap[(deviceString)])
                }

            }

            logger.info("ChooseDevicePrompt Prompt Execution End")
        }
    }
}
