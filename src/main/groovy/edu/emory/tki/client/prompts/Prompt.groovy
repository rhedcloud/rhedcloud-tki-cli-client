package edu.emory.tki.client.prompts

import edu.emory.tki.client.state.StateManager
import org.beryx.textio.TextIO
import org.springframework.beans.factory.annotation.Autowired

/**
 * Base for all Prompts.
 */
abstract class Prompt {

    @Autowired
    protected TextIO textIO

    @Autowired
    protected StateManager stateManager

    abstract void prompt()
}
