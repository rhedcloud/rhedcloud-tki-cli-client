package edu.emory.tki.client.prompts

import edu.emory.tki.client.state.StateEnum
import org.rhedcloud.www.tkiservice.Device
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Prompt for DUO authentication factor.
 */
@Component
class ChooseAuthFactorPrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(ChooseAuthFactorPrompt.class)

    void prompt() {

        if (stateManager.get(StateEnum.S_PRINCIPAL) && stateManager.get(StateEnum.O_CHOSEN_DEVICE) && !stateManager.get(StateEnum.S_AUTH_FACTOR)) {
            logger.info("ChooseAuthFactor Prompt Execution Start")

            Device device = stateManager.get(StateEnum.O_CHOSEN_DEVICE) as Device

            if (device.capability.length == 1) {
                logger.info("Only one device capability choosing ${device.capability[0]}")
                // no need to choose when there is only one
                stateManager.put(StateEnum.S_AUTH_FACTOR, device.capability[0])
            } else {
                List<String> capabilities = device.capability.toList()
                if (capabilities.contains('mobile_otp')) {
                    capabilities.remove('mobile_otp') //we don't support mobile_otp
                }

                if (stateManager.get(StateEnum.B_AUTO_PICK_DUO_AUTH_DEVICES)) {

                    logger.info("Auto parameter is set,  choosing ${device.capability[0]}")
                    stateManager.put(StateEnum.S_AUTH_FACTOR, capabilities.get(0))

                } else {

                    String authFactor = textIO.newIntInputReader()
                            .withNumberedPossibleValues(true)
                            .withPossibleValues(capabilities)
                            .withDefaultValue(capabilities.get(0))
                            .read("Available Duo Authentication Methods")

                    logger.info("Prompted user for authentication factor, user chose ${authFactor}")
                    stateManager.put(StateEnum.S_AUTH_FACTOR, authFactor)
                }
            }

            logger.info("ChooseAuthFactor Prompt Execution End")
        }
    }
}
