package edu.emory.tki.client.prompts

import edu.emory.tki.client.service.LocalFileService
import edu.emory.tki.client.state.StateEnum
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/**
 * Prompt for AWS region.
 */
@Component
class ChooseRegionPrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(ChooseRegionPrompt.class)

    @Value('${tki.client.regions.supported}')
    private String[] supportedRegions

    @Autowired
    private LocalFileService localAwsFileService

    void prompt() {

        if (!stateManager.get(StateEnum.S_REGION)) {
            logger.info("ChooseRegionPrompt Prompt Execution Start")
            List<String> supportedRegionsList = Arrays.asList(supportedRegions)

            if (supportedRegions.length == 1) {

                logger.info("Only one region in config file, choosing ${supportedRegions[0]}")
                // no need to choose when there is only one
                stateManager.put(StateEnum.S_REGION, supportedRegions[0])

            } else if (stateManager.get(StateEnum.S_PRE_SELECTED_REGION)
                    && supportedRegionsList.contains(stateManager.get(StateEnum.S_PRE_SELECTED_REGION))) {

                String region = stateManager.get(StateEnum.S_PRE_SELECTED_REGION)
                logger.info("Region parameter set,  choosing ${region}")
                stateManager.put(StateEnum.S_REGION, region)

            } else {

                String defaultRegionFromFile = localAwsFileService.readDefaultAwsRegion()
                String defaultRegion
                if (supportedRegionsList.contains(defaultRegionFromFile)) {
                    defaultRegion = defaultRegionFromFile
                } else {
                    defaultRegion = supportedRegionsList.get(0)
                }

                String region = textIO.newStringInputReader()
                        .withNumberedPossibleValues(true)
                        .withPossibleValues(supportedRegionsList)
                        .withDefaultValue(defaultRegion)
                        .read("Available Regions")

                logger.info("Prompted user for region, user chose ${region}")
                stateManager.put(StateEnum.S_REGION, region)
            }
            logger.info("ChooseRegionPrompt Prompt Execution End")
        }
    }
}
