package edu.emory.tki.client.prompts

import edu.emory.tki.client.state.StateEnum
import org.rhedcloud.www.tkiservice.RoleDetail
import org.rhedcloud.www.tkiservice.SecurityAssertion
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Prompt for AWS role.
 */
@Component
class ChooseRolePrompt extends Prompt {

    private static final Logger logger = LoggerFactory.getLogger(ChooseRolePrompt.class)

    void prompt() {

        if (stateManager.get(StateEnum.O_SECURITY_ASSERTION) && !stateManager.get(StateEnum.O_ROLE_DETAIL)) {
            logger.info("ChooseRolePrompt Prompt Execution Start")
            SecurityAssertion securityAssertion = stateManager.get(StateEnum.O_SECURITY_ASSERTION) as SecurityAssertion

            if (securityAssertion.roleDetail.length == 1) {
                // no need to choose when there is only one
                logger.info("Only one role available, choosing ${roleDetailDisplayString(securityAssertion.roleDetail[0])}")
                stateManager.put(StateEnum.O_ROLE_DETAIL, securityAssertion.roleDetail[0])
            }
            else {
                Map<String, RoleDetail> roleMap = securityAssertion.roleDetail.collectEntries {
                    [(roleDetailDisplayString(it)): it]
                }

                List<String> roleStrings = new ArrayList<String>(roleMap.keySet()).sort()

                RoleDetail preSelect = null
                if (stateManager.get(StateEnum.S_PRE_SELECTED_ROLE)) {
                    String[] preSelectedRole = (stateManager.get(StateEnum.S_PRE_SELECTED_ROLE) as String).split(',')
                    // shouldn't need to validate length because it was already done in MainView.initArgs but be extra safe
                    if (preSelectedRole.length == 2) {
                        for (RoleDetail rd : securityAssertion.roleDetail) {
                            String arnBit = rd.roleArn.substring(rd.roleArn.indexOf('/') + 1)
                            if (preSelectedRole[0] == rd.accountAlias.name && preSelectedRole[1] == arnBit) {
                                preSelect = rd;
                                break;
                            }
                        }
                        if (preSelect) {
                            logger.info("Pre selected role accepted, choosing ${roleDetailDisplayString(preSelect)}")
                        }
                        else {
                            logger.info("Pre selected role not found")
                        }
                    }
                    else {
                        logger.info("Pre selected role set, but has wrong format")
                    }
                }

                if (preSelect) {
                    stateManager.put(StateEnum.O_ROLE_DETAIL, preSelect)
                }
                else {
                    String roleString = textIO.newStringInputReader()
                            .withNumberedPossibleValues(true)
                            .withPossibleValues(roleStrings)
                            .withDefaultValue(roleStrings.get(0))
                            .read("Available Roles")

                    logger.info("Prompted user for role, user chose ${roleString}")
                    stateManager.put(StateEnum.O_ROLE_DETAIL, roleMap[(roleString)])
                }
            }
            logger.info("ChooseRolePrompt Prompt Execution End")
        }
    }
    private static String roleDetailDisplayString(RoleDetail roleDetail) {
        String displayString = "Account: \"${roleDetail.accountAlias.name}\""
        if (roleDetail.accountAlternateName) {
            displayString += " (${roleDetail.accountAlternateName})"
        }
        displayString += ", Role: \"${roleDetail.roleArn.substring(roleDetail.roleArn.indexOf('/') + 1)}\""
        return displayString
    }
}
