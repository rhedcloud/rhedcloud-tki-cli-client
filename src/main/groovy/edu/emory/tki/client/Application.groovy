
package edu.emory.tki.client

import org.beryx.textio.TextIO
import org.beryx.textio.TextIoFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

/**
 * The Application class provides a top-level entry point for any static configuration that needs
 * to run prior to any application logic, such as Bean configuration, State Machine setup, etc.
 */
@SpringBootApplication
class Application {
    /**
     * Main entry point to the application
     * @param args
     */
    static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class)
        app.run(args)
    }


    @Bean
    TextIO textIO() {
        return TextIoFactory.getTextIO()
    }
}