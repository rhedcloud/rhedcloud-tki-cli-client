package edu.emory.tki.client.updater

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.event.ProgressEvent
import com.amazonaws.event.ProgressEventType
import com.amazonaws.event.ProgressListener
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.transfer.Download
import com.amazonaws.services.s3.transfer.TransferManager
import com.amazonaws.services.s3.transfer.TransferManagerBuilder
import edu.emory.tki.client.state.StateEnum
import edu.emory.tki.client.state.StateManager
import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.archivers.ArchiveStreamFactory
import org.beryx.textio.InputReader
import org.beryx.textio.TextIO
import org.beryx.textio.TextTerminal
import org.beryx.textio.console.ConsoleTextTerminal
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.util.FileSystemUtils
import org.springframework.util.StreamUtils

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.util.zip.GZIPInputStream

/**
 * If an update is available, prompt the user to see if they would like to install it.
 * If so, then download the installer directly from the S3 bucket, unpack it, and execute it.
 */
@Component
class UpdateApply {
    private static final Logger logger = LoggerFactory.getLogger(UpdateApply.class)

    // used several times so made a constant
    private static final String MANUAL_INSTALL = "Please install the update at your earliest convenience."

    @Autowired
    protected TextIO textIO

    @Autowired
    private StateManager stateManager

    @Value('${tki.client.env}')
    private String env

    @Value('${tki.client.buildNumber}')
    private String buildNumber

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.accessKeyId}')
    private String accessKeyId

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.secretAccessKey}')
    private String secretAccessKey

    @Value('${tki.client.${tki.client.env}.installerS3bucket}')
    private String installerS3bucket;


    void applyUpdate() {
        Boolean updateAvailable = stateManager.get(StateEnum.B_UPDATE_AVAILABLE)
        if (updateAvailable == null || !updateAvailable) {
            return  // no update available
        }

        TextTerminal terminal = textIO.getTextTerminal()

        println() // provide spacing from credentials message
        terminal.println("A new version of the TKI CLI Client is available.")

        String os = System.getProperty("os.name").toLowerCase()
        def isWindows = os.startsWith("windows")
        def isMac = os.startsWith("mac")
        def isLinux = os.startsWith("linux")

        String installerFilename
        boolean isGzipped, isTar

        if (isWindows) {
            installerFilename = "windows-${env.toLowerCase()}-installer.zip"
            isGzipped = false
            isTar = false
        }
        else if (isMac) {
            installerFilename = "mac-${env.toLowerCase()}-installer.tar.gz"
            isGzipped = true
            isTar = true
        }
        else if (isLinux) {
            installerFilename = "linux-${env.toLowerCase()}-installer.tar.gz"
            isGzipped = true
            isTar = true
        }
        else {
            // can't tell which installer should be run - let the user decide
            terminal.println(MANUAL_INSTALL)
            return
        }

        InputReader applyUpdateReader = textIO.newBooleanInputReader()
                .withTrueInput("y").withFalseInput("n").withDefaultValue("n")
        // the default value comes back as a string, otherwise we get back a Boolean
        Object applyUpdateInput = applyUpdateReader.read("Install update? (Y/N)")
        boolean doInstall = (applyUpdateInput instanceof String) ? (((String) applyUpdateInput) == "Y") : ((Boolean) applyUpdateInput)

        if (!doInstall) {
            terminal.println(MANUAL_INSTALL)
            return
        }

        // store the downloaded installer bundle to this temporary file
        File tempFile = Files.createTempFile("tki-client-installer-${env.toLowerCase()}", isTar ? ".tar" : ".zip").toFile()
        tempFile.deleteOnExit()
        // unpack the installer bundle to this directory
        File tempDir = Files.createTempDirectory("tki-client-installer-${env.toLowerCase()}").toFile()

        try {
            AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretAccessKey)))
                    .withRegion("us-east-1") // installer bundles are in this region
                    .build()

            // report progress of the installer bundle download
            def listener = new ProgressListener() {
                private long totalBytes = 0
                private long progressBytes = 0
                private int lastPct = 0
                private int notifyLevel = 9

                @Override
                void progressChanged(ProgressEvent progressEvent) {
                    synchronized (this) {
                        if (progressEvent.eventType == ProgressEventType.RESPONSE_CONTENT_LENGTH_EVENT) {
                            totalBytes += progressEvent.bytes
                        }
                        else if (progressEvent.eventType == ProgressEventType.RESPONSE_BYTE_TRANSFER_EVENT) {
                            progressBytes += progressEvent.bytes
                        }

                        int pct = (totalBytes == 0) ? 0 : (progressBytes * 100.0 / totalBytes)

                        // due to the async nature of the download,
                        // don't report the same percent more than once and don't exceed 100 percent
                        if (pct > 0 && pct == lastPct) {
                            pct++
                        }
                        lastPct = pct
                        if (pct >= 100) {
                            pct = 99
                        }

                        if (pct > notifyLevel) {
                            if (terminal instanceof ConsoleTextTerminal) {
                                terminal.print(pct + "%% ")
                            } else {
                                terminal.print(pct + "% ")
                            }
                            notifyLevel += 10  // report every 10%
                        }
                    }
                }
            }

            terminal.print("Downloading update.  Percent complete: ")

            TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(s3client).build()
            try {
                Download download = transferManager.download(installerS3bucket, installerFilename, tempFile, 0)
                download.addProgressListener(listener)
                download.waitForCompletion()
            } finally {
                transferManager.shutdownNow()
            }

            if (terminal instanceof ConsoleTextTerminal) {
                terminal.println("100%%")
            } else {
                terminal.println("100%")
            }

            terminal.println("Running installer ...")


            def bis = new BufferedInputStream(new FileInputStream(tempFile))
            def inputStream = isGzipped ? new GZIPInputStream(bis) : bis

            logger.info("unpacking ${installerFilename} to ${tempDir.absolutePath}")

            def archiveFormat = isTar ? ArchiveStreamFactory.TAR : ArchiveStreamFactory.ZIP
            ArchiveInputStream archiveIS = new ArchiveStreamFactory().createArchiveInputStream(archiveFormat, inputStream)
            ArchiveEntry archiveEntry

            try {
                while ((archiveEntry = archiveIS.getNextEntry()) != null) {
                    File outputFile = new File(tempDir, archiveEntry.getName())

                    if (archiveEntry.isDirectory()) {
                        if (!outputFile.exists() && !outputFile.mkdirs()) {
                            throw new Exception("Unable to create directory " + outputFile.absolutePath)
                        }
                    }
                    else {
                        File parent = outputFile.getParentFile()
                        if (!parent.isDirectory() && !parent.mkdirs()) {
                            throw new Exception("Unable to create parent directory " + parent.absolutePath)
                        }
                        Files.copy(archiveIS, outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
                    }
                }
            }
            finally {
                archiveIS.close()
            }


            // based on the platform, figure out the command to run the installer and the working directory
            File workingDirectory
            String[] command

            if (isWindows) {
                workingDirectory = tempDir
                command = ["cmd", "/C", "set_up.exe"]
            }
            else if (isMac) {
                workingDirectory = new File(tempDir.absolutePath, "InstallTkiCliClient.app/Contents/MacOS")
                command = ["sh", "InstallTkiCliClient"]
            }
            else if (isLinux) {
                workingDirectory = tempDir
                command = ["sh", "install.sh"]
            }
            else {
                return  // can't happen but make the linter happy
            }

            ProcessBuilder pb = new ProcessBuilder(command).directory(workingDirectory)
            Map<String, String> processEnvironment = pb.environment()
            processEnvironment.put("JAVA_HOME", System.getProperty("java.home"))

            logger.info("Running installer: " + command + " from working directory " + workingDirectory.absolutePath
                    + " with JAVA_HOME=" + processEnvironment.get("JAVA_HOME"))

            Process p = pb.start()

            def stdout = new ByteArrayOutputStream()
            def stderr = new ByteArrayOutputStream()
            StreamUtils.copy(p.getInputStream(), stdout)
            StreamUtils.copy(p.getErrorStream(), stderr)
            logger.info("Installer stdout: " + stdout.toString())
            logger.info("Installer stderr: " + stderr.toString())

            p.waitFor()

        }
        catch (Exception e) {
            logger.error("Update error", e)

            terminal.println("Error encountered with the update.  See the log.")
            terminal.println(MANUAL_INSTALL)
        }
        finally {
            FileSystemUtils.deleteRecursively(tempDir)
        }
    }
}
