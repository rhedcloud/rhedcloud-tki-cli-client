package edu.emory.tki.client.updater

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.S3Object
import edu.emory.tki.client.state.StateEnum
import edu.emory.tki.client.state.StateManager
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

/**
 * Check to see if a new version of the TKI CLI Client is available.
 * This depends on a step done during the pipeline that writes the VERSION.txt file to the S3 bucket
 *  along with the installation bundles.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class UpdateChecker implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(UpdateChecker.class)

    @Autowired
    private StateManager stateManager

    @Value('${tki.client.env}')
    private String env

    @Value('${tki.client.buildNumber}')
    private String buildNumber

    @Value('${tki.client.brand.name}')
    private String brandName

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.accessKeyId}')
    private String accessKeyId

    @Value('${tki.client.${tki.client.env}.aws.secretsmanager.secretAccessKey}')
    private String secretAccessKey

    @Value('${tki.client.${tki.client.env}.installerS3bucket}')
    private String installerS3bucket;


    @Override
    void run(String... args) throws Exception {
        final String versionFile = "VERSION_" + brandName.trim().replace(" ", "_").toLowerCase() + "_" + env.toLowerCase() + ".txt"
        InputStream objectContent = null
        try {
            AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretAccessKey)))
                    .withRegion("us-east-1") // version file is in this region
                    .build()

            S3Object s3Object = s3client.getObject(installerS3bucket, versionFile)
            objectContent = s3Object.getObjectContent()
            String buildInfo = objectContent.getText()
            logger.info("UpdateChecker: buildNumber is ${buildNumber} and ${versionFile} contents : ${buildInfo}")
            String[] buildBits = buildInfo.split(":")
            String latestClientVersion = buildBits[2].trim()

            stateManager.put(StateEnum.B_UPDATE_AVAILABLE, (buildNumber != latestClientVersion))

        } catch (AmazonS3Exception e) {
            logger.error("Error encountered while checking for latest version in file " + versionFile, e)
        } finally {
            if (objectContent != null) {
                objectContent.close()
            }
        }
    }
}
