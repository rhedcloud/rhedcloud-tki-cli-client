package edu.emory.tki.client.state

import edu.emory.tki.client.service.LocalFileService
import edu.emory.tki.client.service.TkiClientService
import org.rhedcloud.www.tkiservice.Device
import org.rhedcloud.www.tkiservice.RoleAssumption
import org.rhedcloud.www.tkiservice.RoleDetail
import org.rhedcloud.www.tkiservice.SecurityAssertion
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

/**
 * Manage the states that the program can assume.
 */
@Component
class StateManager {

    @Autowired
    private TkiClientService tkiService

    @Autowired
    private LocalFileService localFileService

    @Value('${tki.client.num.credential.retry}')
    private Integer numUserNamePasswordRetries

    @Value('${tki.client.default.assume.role.timeout.seconds}')
    private Integer assumeRoleTimeout

    @Value('${tki.client.num.passcode.retry}')
    private Integer numPasscodeRetries

    private Map<StateEnum, Object> state

    @PostConstruct
    private void init() {
        state = new HashMap<>()
        state.put(StateEnum.I_NUM_RETRIES_USER_NAME_PASSWORD, numUserNamePasswordRetries)
        state.put(StateEnum.I_NUM_RETRIES_PASSCODE, numPasscodeRetries)
        state.put(StateEnum.I_ASSUME_ROLE_DURATION_SECONDS, assumeRoleTimeout)
    }

    Object get(StateEnum item) {
        return state.get(item)
    }

    void putAll(Map<StateEnum, Object> newValue) {
        state.putAll(newValue)
        checkTransitions()
    }

    void put(StateEnum key, Object newValue) {
        state.put(key, newValue)
        checkTransitions()
    }

    private void checkTransitions() {
        checkLogin()
        checkSmsPasscodeSend()
        checkAssumeRoleAndWriteCredentials()
    }

    private void checkLogin() {
        if (state.get(StateEnum.S_PRINCIPAL)
                && state.get(StateEnum.S_PASSWORD)
                && !state.get(StateEnum.O_SECURITY_ASSERTION)) {

            SecurityAssertion securityAssertion =
                    tkiService.samlLogin(state.get(StateEnum.S_PRINCIPAL) as String, state.get(StateEnum.S_PASSWORD) as String)

            state.put(StateEnum.O_SECURITY_ASSERTION, securityAssertion)
        }
    }

    private void checkSmsPasscodeSend() {

        if (state.get(StateEnum.O_CHOSEN_DEVICE)
                && state.get(StateEnum.S_PRINCIPAL)
                && state.get(StateEnum.S_PASSWORD)
                && state.get(StateEnum.S_AUTH_FACTOR) == 'sms'
                && !state.get(StateEnum.B_SMS_PASSCODE_SENT)) {

            tkiService.smsPasscodeRequest(state.get(StateEnum.S_PRINCIPAL) as String,
                    state.get(StateEnum.S_PASSWORD) as String,
                    ((Device) state.get(StateEnum.O_CHOSEN_DEVICE)).deviceId)

            state.put(StateEnum.B_SMS_PASSCODE_SENT, Boolean.TRUE)
        }
    }

    private void checkAssumeRoleAndWriteCredentials() {
        if (state.get(StateEnum.S_REGION)
                && state.get(StateEnum.I_ASSUME_ROLE_DURATION_SECONDS)
                && state.get(StateEnum.S_PRINCIPAL)
                && state.get(StateEnum.S_PASSWORD)
                && state.get(StateEnum.O_CHOSEN_DEVICE)
                && state.get(StateEnum.S_AUTH_FACTOR)
                && state.get(StateEnum.O_ROLE_DETAIL)
                && state.get(StateEnum.B_PASSCODE_ACCEPTED)
                && !state.get(StateEnum.B_CREDENTIALS_WRITTEN)) {

            RoleAssumption roleAssumption =
                    tkiService.assumeRole(
                            state.get(StateEnum.S_PRINCIPAL) as String,
                            state.get(StateEnum.S_PASSWORD) as String,
                            state.get(StateEnum.O_ROLE_DETAIL) as RoleDetail,
                            state.get(StateEnum.O_CHOSEN_DEVICE) as Device,
                            state.get(StateEnum.S_AUTH_FACTOR) as String,
                            state.get(StateEnum.I_PASSCODE) as Integer,
                            state.get(StateEnum.S_REGION) as String,
                            state.get(StateEnum.I_ASSUME_ROLE_DURATION_SECONDS) as Integer)

            RoleDetail roleDetail = state.get(StateEnum.O_ROLE_DETAIL) as RoleDetail
            String alias = roleDetail.accountAlias.name
            String role = roleDetail.roleArn.substring(roleDetail.roleArn.indexOf('/') + 1)

            localFileService.writeAwsRegion(alias, role, state.get(StateEnum.S_REGION) as String)
            localFileService.writeAwsCredentialsSection(alias, role, roleAssumption.credentials, state.get(StateEnum.I_ASSUME_ROLE_DURATION_SECONDS) as Integer)
            localFileService.writeTmpRoleFile(alias, role)

            state.put(StateEnum.B_CREDENTIALS_WRITTEN, Boolean.TRUE)
        }
    }
}
