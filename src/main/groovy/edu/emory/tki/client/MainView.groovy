package edu.emory.tki.client


import edu.emory.tki.client.exceptions.AwsAccountServiceDownException
import edu.emory.tki.client.exceptions.DuoAuthRejectedException
import edu.emory.tki.client.exceptions.DuoAuthTimeoutException
import edu.emory.tki.client.exceptions.DuoPasscodeIncorrectException
import edu.emory.tki.client.exceptions.DuoPreAuthFailedException
import edu.emory.tki.client.exceptions.TkiServiceDownException
import edu.emory.tki.client.exceptions.TokenExpiredException
import edu.emory.tki.client.exceptions.TokenMaxTimeException
import edu.emory.tki.client.exceptions.UserExitException
import edu.emory.tki.client.exceptions.UserNamePasswordIncorrectException
import edu.emory.tki.client.exceptions.ValidationException
import edu.emory.tki.client.prompts.ChooseAuthFactorPrompt
import edu.emory.tki.client.prompts.ChooseDevicePrompt
import edu.emory.tki.client.prompts.ChooseRegionPrompt
import edu.emory.tki.client.prompts.ChooseRolePrompt
import edu.emory.tki.client.prompts.PasscodePrompt
import edu.emory.tki.client.prompts.UserNameAndPasswordPrompt
import edu.emory.tki.client.state.StateEnum
import edu.emory.tki.client.state.StateManager
import edu.emory.tki.client.updater.UpdateApply
import org.apache.axis2.AxisFault
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

/**
 * The main view of the application processes command line arguments and
 * then prompts the user for information and drives the issuance of temporary keys.
 */
@Component
class MainView implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MainView.class)

    @Value('${tki.client.default.assume.role.timeout.seconds}')
    private Integer assumeRoleTimeout

    @Autowired
    private StateManager stateManager

    @Autowired
    private ChooseRegionPrompt chooseRegionPrompt

    @Autowired
    private UserNameAndPasswordPrompt userNameAndPasswordPrompt

    @Autowired
    private ChooseDevicePrompt chooseDevicePrompt

    @Autowired
    private ChooseRolePrompt chooseRolePrompt

    @Autowired
    private ChooseAuthFactorPrompt chooseCapabilityPrompt

    @Autowired
    private PasscodePrompt passcodePrompt

    @Autowired
    private UpdateApply updateApply

    /**
     * Run the application.
     * @param args command line arguments
     */
    @Override
    void run(String... args) {

        try {
            initArgs(args)

            userNameAndPasswordPrompt.prompt()
            chooseDevicePrompt.prompt()
            chooseCapabilityPrompt.prompt()
            chooseRolePrompt.prompt()
            chooseRegionPrompt.prompt()
            passcodePrompt.prompt()

            updateApply.applyUpdate()

        } catch (UserExitException e) {

            logger.info("Program exited on UserExitException Exception: ${e.getMessage()}")
            System.out.println("The program has exited.")
            System.exit(1)

        } catch (DuoPasscodeIncorrectException e) {

            logger.info("Program exited on DuoPasscodeIncorrectException Exception: ${e.getMessage()}")
            System.out.println("Incorrect Passcode.   The program has exited.")
            System.exit(2)

        } catch (UserNamePasswordIncorrectException e) {

            logger.info("Program exited on UserNamePasswordIncorrectException Exception: ${e.getMessage()}")
            System.out.println("Incorrect Username / Password combination.   The program has exited.")
            System.exit(3)

        } catch (DuoAuthRejectedException | DuoPreAuthFailedException e) {

            logger.info("Program exited on DuoAuthRejectedException or DuoPreAuthFailedException Exception: ${e.getMessage()}")
            System.out.println("Duo Authentication rejected.  The program has exited.")
            System.exit(4)

        } catch (DuoAuthTimeoutException e) {

            logger.info("Program exited on Duo Auth Timeout Exception: ${e.getMessage()}")
            System.out.println("Duo Authentication timed out.  Check that the device is powered on.  The program has exited.")
            System.exit(5)

        } catch (TokenExpiredException e) {

            logger.info("Program exited on Token Expired Exception: ${e.getMessage()}")
            System.out.println("The prompt has reached the maximum time allowed to complete.")
            System.out.println("Please complete the prompt faster next time.  The program has exited.")
            System.exit(5)

        } catch (TokenMaxTimeException e) {

            logger.info("Program exited on Token Max Time Exception: ${e.getMessage()}")
            System.out.println(e.getMessage())  // The error message will show the max duration allowed
            System.out.println("The program has exited.")
            System.exit(6)

        } catch (ValidationException e) {

            logger.info("Program exited on Validation Exception: ${e.getMessage()}")
            System.out.println(e.getMessage())  // The error message will show the max duration allowed
            System.out.println("The program has exited.")
            System.exit(7)

        } catch (TkiServiceDownException e) {

            logger.info("Program exited on TKI Service Exception: ${e.getMessage()}")
            System.out.println(e.getMessage())
            System.out.println("The program has exited.")
            System.exit(8)

        } catch (AwsAccountServiceDownException e) {

            logger.info("Program exited on AWS Account Service Exception: ${e.getMessage()}")
            System.out.println(e.getMessage())
            System.out.println("The program has exited.")
            System.exit(9)

        } catch (AxisFault e) {

            logger.error("A server exception has occurred: ${e.getMessage()}")
            System.out.println("A server exception has occurred. Please try again later.  The program has exited.")
            System.exit(-2)

        } catch (Exception e) {
            logger.error("An uncaught error has occurred. ", e)
            System.out.println("An error has occurred.  The program has exited.  Please check the logs directory for more details.")
            System.exit(-1)
        }
    }

    private void initArgs(String... args) {
        CliBuilder cliBuilder = createCommandlineBuilder()
        OptionAccessor options = cliBuilder.parse(args)

        if (!options) {
            return
        }

        Map<StateEnum, Object> params = new HashMap<>()

        if (options.h) {
            cliBuilder.usage()
            throw new UserExitException("Exit because of usage display")
        }

        if (options.u) {
            params.put(StateEnum.S_PRINCIPAL, options.u)
            logger.info("User name command line parameter set as ${options.u}")
        }

        if (options.p) {
            params.put(StateEnum.S_PASSWORD, options.p)
            logger.info("Password command line parameter has been set")
        }

        if (options.a) {
            params.put(StateEnum.B_AUTO_PICK_DUO_AUTH_DEVICES, Boolean.TRUE)
            logger.info("Auto command line parameter has been set")
        }

        if (options.r) {
            params.put(StateEnum.S_PRE_SELECTED_REGION, options.r)
            logger.info("Region command line parameter is set as ${options.r}")
        }

        if (options.R) {
            params.put(StateEnum.S_PRE_SELECTED_ROLE, options.R)
            String [] aliasAndRole = ((String) options.R).split(",")
            if (aliasAndRole.length != 2) {
                throw new ValidationException("Role parameter is incorrectly formatted")
            }
            logger.info("Role command line parameter is set as alias=${aliasAndRole[0]} role=${aliasAndRole[1]}")
        }

        if (options.d) {
            if (options.d.isInteger() &&  options.d.toInteger() > 0) {
                params.put(StateEnum.I_ASSUME_ROLE_DURATION_SECONDS, options.d.toInteger())
                logger.info("Duration command line parameter is set as ${ options.d.toInteger()}")
            } else {
                throw new ValidationException("Duration parameter must be set to be greater than 0")
            }
        }

        stateManager.putAll(params)
    }

    private CliBuilder createCommandlineBuilder() {
        CliBuilder cliBuilder = new CliBuilder(
                usage: 'tki [<options>]'
        )

        cliBuilder.width = 80  // number of columns for usage message - default is 74
        cliBuilder.with {
            h(longOpt: 'help', 'Print this help text and exit.')
            u(longOpt: 'username', args: 1, argName: 'USERNAME', 'Username to login with.')
            p(longOpt: 'password', args: 1, argName: 'PASSWORD', 'Password to login with.')
            a(longOpt: 'auto', 'Use all defaults for Duo Authentication')
            r(longOpt: 'region', args: 1, argName: 'REGION', 'Region to use for AWS.  String must match what is available or program will prompt for the region when there is more than one available region')
            R(longOpt: 'role', args: 1, valueSeparator: ',', argName: 'ACCOUNT,ROLE', 'Account and role to use for AWS.  String must match what is available or program will prompt for the role when there is more than one available role')
            d(longOpt: 'duration', args: 1, argName: 'TIMEOUT', "Duration in seconds that the assumed temporary AWS credentials will be valid.  Defaults to ${assumeRoleTimeout} seconds.")
        }

        return cliBuilder
    }
}
